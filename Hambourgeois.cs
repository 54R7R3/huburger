﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuburgerOnTheGo
{
    class Hambourgeois
    {
        //Attributs
        Pain dessus, dessous;
        Bacon bacon;
        Champignon champi;
        Tomate tomate;
        Laitue salade;
        Avocat avoc;
        Galette viande1, viande2;
        string type;            //Type de burger - Hambourgeois Deluxe, Huburger géant, etc.
        double prix;            //Coût du burger

        // Constructeur
        public Hambourgeois(Pain leDessus, Galette laViande1, Pain leDessous, double cout)
        {
            dessous = leDessus;
            viande1 = laViande1;
            dessous = leDessous;
            prix = cout;
        }
        public Hambourgeois(Pain leDessus, Tomate laTomate, Laitue laSalade, Galette laViande1, Pain leDessous, double cout)
        {
            dessous = leDessus;
            tomate = laTomate;
            salade = laSalade;
            viande1 = laViande1;
            dessous = leDessous;
            prix = cout;
        }
        public Hambourgeois(Pain leDessus, Tomate laTomate, Laitue laSalade, Bacon leBacon, Champignon leChampi, Galette laViande1, Galette laViande2, Pain leDessous, double cout)
        {
            dessous = leDessus;
            tomate = laTomate;
            salade = laSalade;
            bacon = leBacon;
            champi = leChampi;
            viande1 = laViande1;
            viande2 = laViande2;
            dessous = leDessous;
            prix = cout;
        }
        public Hambourgeois(Pain leDessus, Champignon leChampi, Avocat leAvoc, Galette laViande1, Pain leDessous, double cout)
        {
            dessous = leDessus;
            champi = leChampi;
            viande1 = laViande1;
            dessous = leDessous;
            prix = cout;
        }

        /// <summary>
        /// Méthode pour calculer le nombre total de calories dans le hambourgeois
        /// </summary>
        /// <returns></returns>
        public int CalculerNbCalories()
        {
            int nbCalories = 0;

            nbCalories += dessus.GetNbCalories();
            nbCalories += bacon.GetNbCalories();
            nbCalories += champi.GetNbCalories();
            nbCalories += tomate.GetNbCalories();
            nbCalories += salade.GetNbCalories();
            nbCalories += avoc.GetNbCalories();
            nbCalories += viande1.GetNbCalories();
            nbCalories += viande2.GetNbCalories();
            nbCalories += dessous.GetNbCalories();

            return nbCalories;
        }
        /// <summary>
        /// Méthode poure obtenir le prix du hambourgeois
        /// </summary>
        /// <returns>Le prix</returns>
        public double GetPrix()
        {
            return prix;
        }
        /// <summary>
        /// Méthode pour imprimer en console le type d'hambourgeois, le prix et les calories
        /// </summary>
        public virtual void Imprimer()
        {
            Console.WriteLine(type + "\t" + prix + "\t" + CalculerNbCalories());
        }
    }
}
