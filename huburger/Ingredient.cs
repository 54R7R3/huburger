﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Ingredient.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace HuburgerOnTheGo
{
    class Ingredient
    {
        // Attribue
        protected int calories;

        // Constructeur
        public Ingredient() 
        {
            calories = 0;
        }
        public int GetNbCalories() { return calories; }
    }
}
