﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Facture.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuburgerOnTheGo
{
    /// <summary>
    /// Classe générant une facture
    /// </summary>
    class Facture
    {
        /* Attributs */
        const double TPS = 0.05;
        const double TVQ = 0.09975;

        /// <summary>
        /// Méthode pour imprimer une facture comportant des hambourgeois
        /// </summary>
        /// <param name="lesHam">Le t'ableau d'ahambourgeois à imprimer</param>
        public void Imprimer(Hambourgeois[] lesHam)
        {
            double sousTot;
            int caloriesT;
            Console.Clear();
            Console.WriteLine("\n\n\n\t*** DÉBUT DE LA FACTURE ***");
            Console.WriteLine("Huburger on the Go --- Facture Client");
            Console.WriteLine("Liste produits\t\tPrix\tNb Calories\n");

            //Afficher la liste des aliments dans la commande
            AfficherListeHambourgeois(lesHam);
            //Calculer les montants et le total des calories
            sousTot = CalculerSousTotal(lesHam);
            caloriesT = CalculerCaloriesTotal(lesHam);
            //Afficher les informations sommaires de la facture
            AfficherSommaire(sousTot, caloriesT);

            Console.WriteLine("\n\t*** FIN DE LA FACTURE ***");
        }
        /// <summary>
        /// Afficher le sommaire des coûts de la facture
        /// </summary>
        /// <param name="sousT">Le montant du sous-total</param>
        /// <param name="caloriesTotal">Le nombre de calories totales</param>
        private void AfficherSommaire(double sousT, int caloriesTotal)
        {
            double valTPS, valTVQ;
            Console.WriteLine("Sous-Total : \t\t{0}$", sousT);
            valTPS = CalculerTaxe(sousT, TPS);
            Console.WriteLine();
            Console.WriteLine("TPS ({0})% : \t\t\t{1}", TPS * 100, valTPS);
            valTVQ = CalculerTaxe(sousT, TVQ);
            Console.WriteLine("TVQ ({0})% : \t{0}", TVQ * 100, valTVQ);

            Console.WriteLine("Total : \t{0}$\n", Math.Round(sousT + valTPS + valTVQ, 2));
            Console.WriteLine("Le nombre total de calories est : {0}", caloriesTotal);
        }
        /// <summary>
        /// Calculer le prix total des hambourgeois
        /// </summary>
        /// <param name="lesHam">Le tableau d'hambourgeois</param>
        /// <returns>Le coût total</returns>
        private double CalculerSousTotal(Hambourgeois[] lesHam)
        {
            int i = 0;
            double total = 0;
            while (i < lesHam.Length && lesHam[i] != null)
            {
                total += lesHam[i].GetPrix();
                i++;
            }
            return Math.Round(total, 2);
        }
        /// <summary>
        /// Calculer le total du montant accordé aux taxes
        /// </summary>
        /// <param name="montant">Le sous-total</param>
        /// <param name="taxes">La valeur de la taxe</param>
        /// <returns>Le montant résultant de la taxe</returns>
        private double CalculerTaxe(double montant, double taxe)
        {
            montant = montant * taxe;
            return Math.Round(montant, 2);
        }
        /// <summary>
        /// Calculer le nombre total de calories sur la facture
        /// </summary>
        /// <param name="lesHam">Le tableau d'hambourgeois</param>
        /// <returns>Le nombre total de calories</returns>
        private int CalculerCaloriesTotal(Hambourgeois[] lesHam)
        {
            int i = 0, total = 0;
            while (i < lesHam.Length && lesHam[i] != null)
            {
                total += lesHam[i].CalculerNbCalories();
                i++;
            }
            return total;
        }
        /// <summary>
        /// Imprimer les informations sur les hambourgeois
        /// </summary>
        /// <param name="lesHam">Le tableau d'hambourgeois</param>
        private void AfficherListeHambourgeois(Hambourgeois[] lesHam)
        {
            int i = 0;
            Console.WriteLine("--HAMBOURGEOIS--");
            while(i < lesHam.Length && lesHam[i] != null)
            {
                lesHam[i].Imprimer();
                i++;
            }
        }
    }
}
