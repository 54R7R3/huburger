﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  GestionHambourgeois.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuburgerOnTheGo
{
    /// <summary>
    /// Classe effectuant l'ajout d'hambourgeois afin de produire une facture
    /// </summary>
    class GestionHambourgeois
    {
        /// <summary>
        /// Cette fonction affiche le menu et permet à l'utilisateur de sélectionner 
        /// ce qu'il désire commander. Il termine par l'impression d'une facture 
        /// en affichage console.
        /// </summary>
        public void MenuGestion()
        {
            Facture laFacture = new Facture();
            Console.WriteLine("Bienvenu chez Huburger on the Go!\nAppuyez sur Entrée pour commencer.");
            Hambourgeois[] tabHambourgeois = new Hambourgeois[50];
            int nbHamAjoutes = 0;
            bool encore = true;
            do
            {
                Console.Clear();
                AfficherMenu();
                switch (EntrerEtValiderNombre(1, 5))
                {
                    case 1:
                        {
                            Pain painDessus = new Pain();
                            Pain painDessous = new Pain();
                            Galette viande = new Galette();
                            double cout = 6.99;
                            tabHambourgeois[nbHamAjoutes] = new Hambourgeois(painDessus, viande, painDessous, cout);
                            nbHamAjoutes++;
                            break; 
                        }
                    case 2:
                        {
                            Pain painDessus = new Pain();
                            Pain painDessous = new Pain();
                            Tomate tomate = new Tomate();
                            Laitue laitue = new Laitue();
                            Galette viande = new Galette();
                            double cout = 11.99;
                            tabHambourgeois[nbHamAjoutes] = new Hambourgeois(painDessus, tomate, laitue, viande, painDessous, cout);
                            nbHamAjoutes++;
                            break;
                        }
                    case 3:
                        {
                            Pain painDessus = new Pain();
                            Pain painDessous = new Pain();
                            Tomate tomate = new Tomate();
                            Laitue laitue = new Laitue();
                            Galette viande1 = new Galette();
                            Galette viande2 = new Galette();
                            Bacon bacon = new Bacon();
                            Champignon champignon = new Champignon();
                            double cout = 15.35;
                            tabHambourgeois[nbHamAjoutes] = new Hambourgeois(painDessus, tomate, laitue, bacon,
                            champignon, viande1, viande2, painDessous, cout);
                            nbHamAjoutes++;
                            break;
                        }
                    case 4:
                        {
                            Pain painDessus = new Pain();
                            Pain painDessous = new Pain();
                            Champignon champignon = new Champignon();
                            Avocat avocat = new Avocat();
                            Galette viande = new Galette();
                            double cout = 12.49;
                            tabHambourgeois[nbHamAjoutes] = new Hambourgeois(painDessus, champignon, avocat, viande, painDessous, cout);
                            nbHamAjoutes++;
                            break;
                        }
                    case 5:
                        {
                            encore = false;
                            break;
                        }
                    default: break;
                }
                if(encore)
                {
                    Console.Write("\nDésirez-vous ajouter un autre item à votre commande? (O/N) : ");
                }
            } while (encore && EntrerEtValiderOuiNon() == 'O'); //Si la variable à gauche du '&&' est fausse, le programme cesse d'exécuter le reste
                                                                //de la condition car la condition sera fausse de toute façon.

            Console.WriteLine("\nCommande complétée.");
            laFacture.Imprimer(tabHambourgeois);
            Console.WriteLine("\n\nAu revoir!\t\t\t(C)Huburger on the Go, 2020");
        }
        /// <summary>
        /// Affiche les choix du menu.
        /// </summary>
        private void AfficherMenu()
        {
            Console.WriteLine("Quel hambourgeois désirez-vous commander?");
            Console.WriteLine("\t1- Hambourgeois régulier\n\t2- Hambourgeois Deluxe\n\t3- Huburger Géant\n\t4- Hambourgeois Fancy\n\t5- Rien.");
            Console.Write("Votre choix : ");
        }
        /// <summary>
        /// Permet l'entrée et la validation d'un nombre entré par l'utilisateur
        /// </summary>
        /// <param name="min">Minimum de la validation de nombres entiers</param>
        /// <param name="max">Maximum de la validation de nombres entiers</param>
        /// <returns>Retourne un entier valide entre min et max</returns>
        private int EntrerEtValiderNombre(int min, int max)
        {
            int rep;
            bool valid = false, isOK;
            do
            {
                isOK = Int32.TryParse(Console.ReadLine(), out rep);
                if (isOK && (rep < min || rep > max))
                    Console.Write("\n**** Choix invalide. Entrez un nouveau choix : ");
                else
                    valid = true;
            } while (!valid);
            return rep;
        }
        /// <summary>
        /// Permet l'entrée et la validation d'un choix Oui/Non par l'utilisateur
        /// </summary>
        /// <returns>Retourne la lettre du choix en majuscule (O/N)</returns>
        private char EntrerEtValiderOuiNon()
        {
            char rep;
            bool valid = false, isOK;
            do
            {
                isOK = Char.TryParse(Console.ReadLine(), out rep);
                if (isOK && (char.ToUpper(rep) == 'O' || char.ToUpper(rep) == 'N'))
                    valid = true;
                else
                    Console.Write("**** Choix invalide. Entrez un nouveau choix (O/N) : ");
            } while (!valid);
            return char.ToUpper(rep);
        }
    }
}
