﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Pain.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace HuburgerOnTheGo
{
    class Pain : Ingredient
    {
        // Constructeur
        public Pain()
        {
            base.calories = 176;
        }
    }
}
