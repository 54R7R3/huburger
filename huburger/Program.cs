﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Program.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuburgerOnTheGo
{
    class Program
    {
        static void Main(string[] args)
        {
            GestionHambourgeois gestionnaire = new GestionHambourgeois();
            gestionnaire.MenuGestion();
            Avocat avoc = new Avocat();
            Console.ReadKey();
        }
    }
}
