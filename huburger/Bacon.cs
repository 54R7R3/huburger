﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Bacon.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace HuburgerOnTheGo
{
    class Bacon : Ingredient
    {
        
        // Constructeur
        public Bacon()
        {
            base.calories = 88;
        }
    }
}
