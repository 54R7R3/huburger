﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Avocat.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace HuburgerOnTheGo
{
    class Avocat : Ingredient
    {
        // Constructeur
        public Avocat()
        {
            base.calories = 15;
        }
    }
}
