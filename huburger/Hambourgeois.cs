﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Hambourgeois.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuburgerOnTheGo
{
    class Hambourgeois
    {
        //Attributs
        Pain dessus, dessous;
        Bacon bacon;
        Champignon champi;
        Tomate tomate;
        Laitue salade;
        Avocat avoc;
        Galette viande1, viande2;
        string type;            //Type de burger - Hambourgeois Deluxe, Huburger géant, etc.
        double prix;            //Coût du burger

        // Constructeur
        public Hambourgeois(Pain leDessus, Galette laViande1, Pain leDessous, double cout)
        {
            dessus = leDessus;
            viande1 = laViande1;
            dessous = leDessous;
            prix = cout;
            type = "Hambourgeois Régulier";
        }
        public Hambourgeois(Pain leDessus, Tomate laTomate, Laitue laSalade, Galette laViande1, Pain leDessous, double cout)
        {
            dessus = leDessus;
            tomate = laTomate;
            salade = laSalade;
            viande1 = laViande1;
            dessous = leDessous;
            prix = cout;
            type = "Hambourgeois Deluxe";
        }
        public Hambourgeois(Pain leDessus, Tomate laTomate, Laitue laSalade, Bacon leBacon, Champignon leChampi, Galette laViande1, Galette laViande2, Pain leDessous, double cout)
        {
            dessus = leDessus;
            tomate = laTomate;
            salade = laSalade;
            bacon = leBacon;
            champi = leChampi;
            viande1 = laViande1;
            viande2 = laViande2;
            dessous = leDessous;
            prix = cout;
            type = "Huburger Géant";
        }
        public Hambourgeois(Pain leDessus, Champignon leChampi, Avocat leAvoc, Galette laViande1, Pain leDessous, double cout)
        {
            dessus = leDessus;
            champi = leChampi;
            viande1 = laViande1;
            dessous = leDessous;
            avoc = leAvoc;
            prix = cout;
            type = "Hambourgeois Fancy";
        }

        /// <summary>
        /// Méthode pour calculer le nombre total de calories dans le hambourgeois
        /// </summary>
        /// <returns></returns>
        public int CalculerNbCalories()
        {
            int nbCalories = 0;

            if (dessus != null) { nbCalories += dessus.GetNbCalories(); }
            if (bacon != null) { nbCalories += bacon.GetNbCalories(); }
            if (champi != null) { nbCalories += champi.GetNbCalories(); }
            if (tomate != null) { nbCalories += tomate.GetNbCalories(); }
            if (salade != null) { nbCalories += salade.GetNbCalories(); }
            if (avoc != null) { nbCalories += avoc.GetNbCalories(); }
            if (viande1 != null) { nbCalories += viande1.GetNbCalories(); }
            if (viande2 != null) { nbCalories += viande2.GetNbCalories(); }
            if (dessous != null) { nbCalories += dessous.GetNbCalories(); }

            return nbCalories;
        }
        /// <summary>
        /// Méthode poure obtenir le prix du hambourgeois
        /// </summary>
        /// <returns>Le prix</returns>
        public double GetPrix()
        {
            return prix;
        }
        /// <summary>
        /// Méthode pour imprimer en console le type d'hambourgeois, le prix et les calories
        /// </summary>
        public void Imprimer()
        {
            if (type == "Huburger Géant")
                Console.WriteLine(type + "\t" + "\t" + prix + "\t" + CalculerNbCalories());
            else
                Console.WriteLine(type + "\t" + prix + "\t" + CalculerNbCalories());
        }
    }
}
