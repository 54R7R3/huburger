﻿//Noms:     Alex Riverin et Mathieu Houde
//Cours:    Programmation Orientée Objet
//Prof.:    Hugo Deschênes
//Fichier:  Galette.cs
//Date:     4 Mars 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace HuburgerOnTheGo
{
    class Galette : Ingredient
    {
        // Constructeur
        public Galette()
        {
            base.calories = 335;
        }
    }
}
